<h1 align="center">clicker</h1>

[![Crates.io](https://img.shields.io/crates/v/clicker.svg)](https://crates.io/crates/clicker)

## About
clicker is a simple desktop automation tool

## How to run

Install clicker through cargo:
```bash
cargo install clicker
```
Run the binary:
```bash
clicker -k <key>
```
If you want options on how to customize clicker, run the following:
```bash
clicker --help
```

## License
MIT
