use inputbot::{KeybdKey, MouseButton};
use snafu::Snafu;

pub type Result<'a, T> = std::result::Result<T, Error<'a>>;

#[derive(Debug, Snafu)]
pub enum Error<'a> {
	#[snafu(display("\"{}\" is not a valid key", value))]
	KeyParseError { value: &'a str },
	#[snafu(display("\"{}\" is not a valid mouse button", value))]
	MouseButtonParseError { value: &'a str },
}

macro_rules! gen_parser {
	($value:expr; $enum:ident; $suffix:expr; $($key:ident),*) => {
		$(
			if stringify!($key).eq_ignore_ascii_case(&(($value).to_string() + $suffix)) {
				return Ok($enum::$key)
			}
		)*
	};
}

pub(crate) fn keycode(value: &str) -> Result<KeybdKey> {
	gen_parser! { value; KeybdKey; "Key"; BackspaceKey, TabKey, EnterKey, EscapeKey, SpaceKey, HomeKey, LeftKey, UpKey, RightKey, DownKey, InsertKey, DeleteKey, Numrow0Key, Numrow1Key, Numrow2Key, Numrow3Key, Numrow4Key, Numrow5Key, Numrow6Key, Numrow7Key, Numrow8Key, Numrow9Key, AKey, BKey, CKey, DKey, EKey, FKey, GKey, HKey, IKey, JKey, KKey, LKey, MKey, NKey, OKey, PKey, QKey, RKey, SKey, TKey, UKey, VKey, WKey, XKey, YKey, ZKey, Numpad0Key, Numpad1Key, Numpad2Key, Numpad3Key, Numpad4Key, Numpad5Key, Numpad6Key, Numpad7Key, Numpad8Key, Numpad9Key, F1Key, F2Key, F3Key, F4Key, F5Key, F6Key, F7Key, F8Key, F9Key, F10Key, F11Key, F12Key, F13Key, F14Key, F15Key, F16Key, F17Key, F18Key, F19Key, F20Key, F21Key, F22Key, F23Key, F24Key, NumLockKey, ScrollLockKey, CapsLockKey, LShiftKey, RShiftKey, LControlKey, RControlKey }

	Err(Error::KeyParseError { value })
}

pub(crate) fn mousebutton(value: &str) -> Result<MouseButton> {
	gen_parser! { value; MouseButton; "Button"; LeftButton, MiddleButton, RightButton, X1Button, X2Button }

	Err(Error::MouseButtonParseError { value })
}
